from hashlib import md5
import sys
import base64
import json


class RSync(object):

	"""docstring for RSync"""

	def __init__(self, f, blocksize):
		self.file = f
		self.blocksize = blocksize
		self.location = 0
		self.M = 1 << 16

	def signature(self):
		'''takes a file f and calculates both the rolling checksum and the block hash and returns
		an iterator from which the lookup table can be built'''
		siglist = []
		while True:
			blockdata = self.file.read(self.blocksize)
			if not blockdata:
				break
			yield [self.blockrolling(blockdata), md5(blockdata).digest()]

	def compare(self, lookup):
		'''takes a second checksum object and compares the current one with it. The return is an
		iterator than can be used to establish the delta.'''
		ablock = self.getblock()
		rollingcs = self.blockrolling(ablock)
		print('first block', ablock)
		print('rolling checksum', rollingcs)

		while rollingcs:
			# if the block hashes are in the lookup, we have a match so emit the block number
			if rollingcs in lookup:
				hashcs = md5(ablock).digest()
				print('found hash, checking digest', hashcs)
				if hashcs in lookup[rollingcs]:
					print("Found strong", (self.location - self.blocksize), lookup[rollingcs][hashcs])
					yield {'OK': lookup[rollingcs][hashcs]}
					ablock = self.getblock()
					rollingcs = self.blockrolling(ablock)
					continue

			# if we didn't get a match, then we step over the bytes, generating a delta stream
			nextbyte = self.getblock(1)
			if nextbyte:
				# print("Searching", (self.location - self.blocksize), ablock[0])
				self.a = (self.a - ablock[0] + nextbyte[0]) % self.M
				self.b = (self.b - (self.blocksize * ablock[0]) + self.a) % self.M
				yield {'search': ablock[0]}
				ablock = ablock[1:] + nextbyte
				rollingcs = self.a + (self.b << 16)
			else:
				for b in ablock:
					yield {'search': b}
				rollingcs = None

	def blockrolling(self, bytelist):
		'''an implementation of the original cheap checksum'''
		blocklen = len(bytelist)
		self.a = sum(bytelist) % self.M
		self.b = sum((blocklen - i) * X for i, X in enumerate(bytelist)) % self.M
		return self.a + (self.b << 16)

	def getblock(self, size=None):
		'''retrieves either a block of data from the file of the length specific when this
		RSync object was instantiated, or of length specified by size'''
		if size == None:
			size = self.blocksize
		self.location += size
		return self.file.read(size)

	def generatesig(self):
		'''generates and returns the lookup table for the data, so that it can be used to 
		compare with a different instance of the file'''
		self.file.seek(0)
		lookup = {}
		for blocknum, r in enumerate(self.signature()):
			if r[0] not in lookup:
				lookup[r[0]] = {}
			lookup[r[0]][r[1]] = blocknum
		return lookup

	def generatesigjson(self):
		fortx = []
		sig = self.generatesig()
		for key in sig:
			hashvalues = sig[key]
			bytesig = list(hashvalues.keys())[0]
			segment = {'key': key, 'sig': base64.b64encode(bytesig).decode('ascii'), 'hash': hashvalues[bytesig]}
			fortx.append(segment)
		return json.dumps(fortx)

	def generatediff(self, lookup):
		search = bytearray()
		actions = []
		for res in self.compare(lookup):
			if 'OK' in res:
				if search:
					actions.append(search)
				search = bytearray()
				actions.append(res['OK'])
			elif 'search' in res:
				search.append(res['search'])

		if search:
			actions.append(search)

		return actions

	def generatediffjson(self, lookup):
		search = bytearray()
		actions = []
		for res in self.compare(lookup):
			if 'OK' in res:
				if search:
					actions.append(base64.b64encode(search).decode('ascii'))
				search = bytearray()
				actions.append(res['OK'])
			elif 'search' in res:
				search.append(res['search'])

		if search:
			actions.append(base64.b64encode(search).decode('ascii'))

		return actions

	def merge(self, actions, fileout):
		lastblock = 0
		for action in actions:
			if type(action) == int:
				while lastblock < action:
					self.file.read(self.blocksize)
					lastblock += 1
				print("copying", lastblock)
				fileout.write(self.file.read(self.blocksize))
				lastblock += 1
			elif type(action) == bytearray:
				fileout.write(action)
			else:
				raise Exception("Unknown type in actions list: " + str(type(action)))	


if __name__ == '__main__':
	testfiles = [ # the first file is the one we want, file 3 is the output
		['sunsetA.jpg', 'sunsetB.jpg', 'sunsetA2.jpg'],
		['HolmesShortA.txt', 'HolmesShortB.txt', 'HolmesShortA2.txt'],
	]

	block_size = 1024

	for files in testfiles:
		fileA = open('./testdata/' + files[0], 'rb') # has the edit
		fileB = open('./testdata/' + files[1], 'rb')

		# assume that B is remote, so fetch the signatures
		checksumA = RSync(fileA, block_size)
		checksumB = RSync(fileB, block_size)
		blookup = checksumB.generatesig()

		# the lookup would be passed over the wire from B to A
		# now compare the signature of B with fileA
		diffs = checksumA.generatediff(blookup)

		# lets do a merge...as if it were on the remote
		fileB2 = open('./testdata/' + files[1], 'rb')
		checksumB2 = RSync(fileB2, block_size)
		fileA2 = open('./testdata/' + files[2], 'wb') # this is our output
		checksumB2.merge(diffs, fileA2)

 