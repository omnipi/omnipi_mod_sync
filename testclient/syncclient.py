import urllib.request
import urllib.parse
import sys
import json
import os
import base64

sys.path.append("../")
sys.path.append('../../../libraries')

from rsync import RSync
import crypty.radix64 as r64


if os.path.exists('.config.json'):
   with open('.config.json', 'r') as f_config_settings:
      try:
         config_settings = json.loads(f_config_settings.read())
         block_size = config_settings['block_size']
         remote_clump = config_settings['remote_clump']
         sync_data = config_settings['sync_data']
      except Exception as e:
         print('Error:', e)
         sys.exit()
else:
   print('Unable to locate a config...')
   sys.exit()


for syncfile in sync_data['files']:
   file_name = syncfile['name']
   file_local_path = syncfile['path']
   file_path = os.path.join(file_local_path, file_name)

   # we're going to be syncing a local test file with the server
   thisFile = open(file_path, 'rb')
   stats = os.stat(file_path)

   # wrap the file in RSync
   localCheckSum = RSync(thisFile, block_size)

   # generate the local sig for the file
   jsontx = localCheckSum.generatesigjson()

   # send the data to OmniPi/Sync/check
   url = 'http://localhost:8080/sync/check'
   request_data = {
      'filesigs': jsontx, 
      'filename': file_name, 
      'localpath': file_local_path, 
      'modifiedtime': stats.st_mtime,
      'blocksite': block_size
   }
   data = urllib.parse.urlencode(request_data)
   data = data.encode('ascii')

   # proxy_support = urllib.request.ProxyHandler({'http':'http://localhost:3128'})
   proxy_support = urllib.request.ProxyHandler({})
   opener = urllib.request.build_opener(proxy_support)
   urllib.request.install_opener(opener)

   request = urllib.request.Request(url, data)
   print("Response")
   with urllib.request.urlopen(request) as response:
      the_page = response.read().decode('utf8')
      print(the_page)
      response_data = json.loads(the_page)
      if 'diff' in response_data:
            difflist = response_data['diff']
            norm_diff = []
            for df in difflist:
               if isinstance(df, str):
                  norm_diff.append(bytearray(base64.b64decode(df)))
               else:
                  norm_diff.append(df)

            with open('./temp_{}.dat'.format(file_name), 'wb') as tempfile:
               localCheckSum.merge(norm_diff, tempfile)
               print('merge here')
            

   # # the lookup would be passed over the wire from B to A
   # # now compare the signature of B with thisFile
   # diffs = localCheckSum.generatediff(blookup)

   # # lets do a merge...as if it were on the remote
   # fileB2 = open('./testdata/' + files[1], 'rb')
   # checksumB2 = RSync(fileB2, block_size)
   # thisFile2 = open('./testdata/' + files[2], 'wb') # this is our output
   # checksumB2.merge(diffs, fileA2)