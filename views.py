from django.http import HttpResponse
from op_django.shortcuts import render, render_to_response
from django.shortcuts import redirect, HttpResponseRedirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt

import json
import os
import time
# import urllib.request as http
import urllib as http
import ophttp.http as mega
import datetime as dt

from mod_sync.rsync import RSync
import crypty.radix64 as r64
import mod_sync.sync_provider as sync_provider

from .forms import UploadFileForm


def index(request):
    context = {}
    context['AppName'] = 'File Sync'
    context['appname'] = 'sync'

    files = sync_provider.fetchfiles()
    filedata = {}
    filedata['root'] = files[0]
    filedata['files'] = files[1]
    filedata['folders'] = files[2]
    context['data'] = filedata

    return render(request, 'sync/index.html', context)


def auth(request):
    context = {}
    context['AppName'] = 'File Sync'
    context['appname'] = 'sync'

    return render(request, 'sync/index.html', context)


def filesets(request):
    context = {}
    context['AppName'] = 'File Sync'
    context['appname'] = 'sync'

    context['file'] = 'File Set One'
    context['lookup'] = '{this-is-a-guid-for-the-fileset}'

    return render(request, 'sync/index.html', context)


def upload(request):
    context = {}
    context['AppName'] = 'File Sync'
    context['appname'] = 'sync'

    return render(request, 'sync/upload.html', context)


def push(request):
    '''data will be accepted by form. Ultimately, this will be encrypted by a stream cipher
    to esnure that the data stream is not able to be easily intercepted, but for now, let's
    just stream it to disk'''
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid:
            with open(os.path.join(settings.OP_FILE_STORE, '__files__', 'sync', 'testupload.dat'), 'wb+') as dest:
                for chunk in request.FILES['file']:
                    dest.write(chunk)
            return HttpResponseRedirect('/sync')
    else:
        # form = UploadFileForm()
        pass


    context = {'form': 'EMPTY'}
    # context.update(csrf(request))
    context['json'] = json.dumps(['this', 'is', 'an', 'array'])
    return render(request, 'omnipi/json.out', context, content_type="application/json")


@csrf_exempt
def check(request):
    '''data will be accepted by form. Ultimately, this will be encrypted by a stream cipher
    to esnure that the data stream is not able to be easily intercepted, but for now, let's
    just stream it to disk'''
    context = {'json': 'not done'}
    status = 200
    if request.method == 'POST':
        if 'filename' in request.POST and 'filesigs' in request.POST and 'modifiedtime' in request.POST:
            syncfilepath = os.path.join(settings.MEDIA_ROOT, 'sync', request.POST.get('localpath', ''), request.POST.get('filename', None))
            if os.path.exists(syncfilepath):
                file_stats = os.stat(syncfilepath)
                with open(syncfilepath, 'rb') as syncfile:
                    localCheckSum = RSync(syncfile, 1024)
                    print("local  file modified date/time", file_stats.st_mtime)
                    print("remote file modified date/time", request.POST.get('modifiedtime'))
                    if file_stats.st_mtime > float(request.POST.get('modifiedtime')):
                        print('need to push to client')
                        blookup = {}
                        filesig = json.loads(request.POST.get('filesigs'))
                        for sig in filesig:
                            blookup[sig['key']] = { bytes(r64.decode(sig['sig'])): sig['hash'] }
                        response = { 'diff': localCheckSum.generatediffjson(blookup) }
                        context['json'] = json.dumps(response)
                        print(context)
                    else:
                        print('need to pull updates')
                        context['json'] = localCheckSum.generatesigjson()
            else:
                context['json'] = { 'message': 'file does not exist', 'serverpath': syncfilepath }
        else:
            raise Exception('Invalid request')

    return render(request, 'omnipi/json.out', context, content_type="application/json", status=status)


def detail(request):
    context = {}
    if 'fid' in request.GET:
        fileRoot = os.path.join(settings.OP_MODULE_ROOT, 'sync', 'testdata')
        if request.GET['fid'] == 'HolmesShort':
            filePath = os.path.join(fileRoot, 'sunsetA.jpg')
            print(filePath)
            context['file'] = 'testdata/HolmesShortA.txt'
            with open(filePath, 'rb') as syncfile:
                checkSumHolmes = rs.RSync(syncfile, 1024)
                fileSig = checkSumHolmes.generatesig()
                checkSumRadix64 = {'filesig': r64.encode(str(fileSig))}
                context['json'] = json.dumps(checkSumRadix64)
        else:
            context['json'] = '''{"status":"500", "message":"sync failed due to unspecified paramete"}'''
    elif request.GET['init'] == 'true':
        initresp = {'deviceid':'184FD3-002DAC2E41-FF03AD'}
        allfiles = {}
        allfiles['sunsetA.jpg'] = { 'modified':'2015-01-14 11:54:23:000Z000', 'size':48356 }
        initresp['allfiles'] = allfiles
        context['json'] = json.dumps(initresp)
        # context['json'] = '''{"status":"200"}'''
    # render(request, 'omnipi/json.out', {'json': json.dumps(context)}, content_type="application/json")
    return render(request, 'omnipi/json.out', context, content_type="application/json")


def op_getinfo():
    return '''The beginnings of a cloud file sync tool.'''