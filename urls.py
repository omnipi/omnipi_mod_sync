from django.conf.urls import patterns, url

from mod_sync import views

urlpatterns = patterns('',
	# ex: /polls/
	url(r'^$', views.index, name='index'),

	url(r'^auth$', views.auth, name='auth'),

    url(r'^filesets$', views.filesets, name='filesets'),

    url(r'^upload$', views.upload, name='upload'),

	url(r'^push$', views.push, name='push'),

	url(r'^check$', views.check, name='check'),

	url(r'^detail$', views.detail, name='detail'),
)