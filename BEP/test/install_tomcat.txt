tomcat6 //IS//Tomcat6 --DisplayName="Apache Tomcat 6" \
	--Install="C:\Program Files\Tomcat\bin\tomcat6.exe" --Jvm=auto \
	--StartMode=jvm --StopMode=jvm \
	--StartClass=org.apache.catalina.startup.Bootstrap --StartParams=start \
	--StopClass=org.apache.catalina.startup.Bootstrap --StopParams=stop