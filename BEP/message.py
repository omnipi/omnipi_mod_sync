import os
import json
import sys

# An attempt to implement the BEPv1 protocol
#	https://github.com/syncthing/specs/blob/master/BEPv1.md

class Message(object):

	def __init__(self):
		self.ver = 0
		self.id = 0
		self.compression = 0

	def serialise(self):
		header1 = (self.ver & 15) << 28 + (self.id & 255) << 16 + (self.messageType & 255) << 8 + (self.compression & 1)
		header2 = 0
		return header1 + header2


class ClusterConfig(Message):

	messageType = 0

	def __init__(self, settings):
		Message.__init__(self)
		self.settings = settings
		self.options = []

	def serialise(self):
		messageHeader = Message.serialise(self)
		return messageHeader


class Index(Message):

	def __init__(self, messageType, folder):
		if messageType == 1 or messageType == 6:
			self.messageType = messageType
		else:
			raise Exception('Invalid type id set for index message. Must be either 1 or 6')
		self.folder = folder

	def serialise(self):
		print('Folder id    :', self.folder.id)
		print('Folder length:', len(self.folder.id))
		for fl in self.folder.files:
			print(">>> " + fl[1])
			print("  File name length:", len(fl[1]))
			# print("  Flags           : ??")
			print("  Modified        :", os.path.getmtime(fl[0]))
			# print("  Version         : ??")
			# print("  Local Version   : ??")
			# print("  Blocks number   : ??")


class Folder(object):

	def __init__(self):
		self.id = ''
		self.devices = []
		self.path = ''
		self.recursive = True

	def setupFromConfig(self, configFolder):
		if 'id' in configFolder and 'path' in configFolder and 'devices' in configFolder and 'recursive' in configFolder:
			self.path = self.setupPath(configFolder['path'])
			self.id = configFolder['id']
			self.recursive = configFolder['recursive']
			self.files = self.makeFileGen()
			self.setupDevices(configFolder['devices'])

	def setupDevices(self, devices):
		for d in devices:
			newDevice = Device()
			newDevice.setupFromConfig(d)
			self.devices.append(newDevice)

	def makeFileGen(self):
		allFiles = os.walk(self.path)
		rootPathLen = len(self.path)
		for ft in allFiles:
			if not self.recursive and not os.path.samefile(ft[0], self.path): raise StopIteration
			for fl in ft[2]:
				filePath = os.path.join(ft[0], fl)
				fileRelPath = filePath[rootPathLen:].replace('\\', '/')
				if fileRelPath[0] == '/': fileRelPath = fileRelPath[1:]
				yield (filePath, fileRelPath)

	def setupPath(self, path):
		if not os.path.exists(path): raise Exception("Unknown or unfindable folder location", path)
		return path
	

class Device(object):

	def __init__(self):
		self.id = ''
		self.flags = 0
		self.max_local_version = 0

	def setupFromConfig(self, configDevice):
		if 'id' in configDevice and 'flags' in configDevice and 'maxver' in configDevice:
			self.id = configDevice['id']
			self.flags = configDevice['flags']
			self.max_local_version = configDevice['maxver']


class Option(object):

	def __init__(self, key, value):
		self.key = key
		self.value = value


class Settings(object):

	def __init__(self):
		self.clientName = ''
		self.clientVersion = ''
		self.folders = []

	def parseSettingsFile(self, config):
		config = json.loads(config)
		if 'clientName' in config: self.clientName = config['clientName']
		if 'clientVersion' in config: self.clientVersion = config['clientVersion']
		if 'folders' in config: self.setupFolders(config['folders'])

	def setupFolders(self, folders):
		for f in folders:
			newFolder = Folder()
			newFolder.setupFromConfig(f)
			self.folders.append(newFolder)

	def fetchFolder(self, folderId):
		for f in self.folders:
			if f.id == folderId:
				return f
		return None


if __name__ == '__main__':

	# create a test settings json file
	configTest = r'{"clientName":"OmniPi Sync","clientVersion":"v0.0.1","folders": [{"id":"1873843798324729","path":"C:\\Developments\\temp\\OP_modules\\sync\\BEP\\test","recursive":true,"devices":[{"id":"NickHomeOmniPi","flags":0,"max_local_version":5}]}]}'
	settings = Settings()
	settings.parseSettingsFile(configTest)

	cConfig = ClusterConfig(settings)
	clusterConfigBlock = cConfig.serialise()
	print(clusterConfigBlock)

	indexMessage = Index(1, settings.fetchFolder('1873843798324729'))
	indexMessage.serialise()