from django.conf import settings
import os


def fetchfiles():
	allfiles = (settings.MEDIA_ROOT, [], [])
	syncroot = os.path.join(settings.MEDIA_ROOT, 'sync')
	for f in os.listdir(syncroot):
		if os.path.isfile(os.path.join(syncroot, f)):
			allfiles[1].append(f)
		else:
			allfiles[2].append(f)
	return allfiles